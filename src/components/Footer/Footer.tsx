import { Button } from '@mui/material';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';

interface FooterProps { }

const Footer: FC<FooterProps> = () => {
    const { t, i18n } = useTranslation();

    function changeLanguage(e: string) {
        i18n.changeLanguage(e);
    }

    return (
        <div className='footer'>
            <Button onClick={() => changeLanguage('en')}>{t('Languages.English')}</Button>
            <Button onClick={() => changeLanguage('es')}>{t('Languages.Spanish')}</Button>
        </div>
    )
}

export default Footer;
