import { FC } from 'react';
import { Route, Routes } from 'react-router';
import Activities from '../../pages/Activities/Activities';
import CommunityAndLeadership from '../../pages/CommunityAndLeadership/CommunityAndLeadership';
import EducationAndCredentials from '../../pages/EducationAndCredentials/EducationAndCredentials';
import Home from '../../pages/Home/Home';
import HonorsAndAwards from '../../pages/HonorsAndAwards/HonorsAndAwards';
import Projects from '../../pages/Projects/Projects';
import WorkExperience from '../../pages/WorkExperience/WorkExperience';

interface MainProps { }

const Main: FC<MainProps> = () => {
    return (
        <Routes>
            <Route path="/*" element={<Home />}></Route>
            <Route path="/JPHeckPages" element={<Home />}></Route>
            <Route path="/JPHeckPages/Projects" element={<Projects />}></Route>
            <Route path="/JPHeckPages/Activities" element={<Activities />}></Route>
            <Route path="/JPHeckPages/CommunityAndLeadership" element={<CommunityAndLeadership />}></Route>
            <Route path="/JPHeckPages/EducationAndCredentials" element={<EducationAndCredentials />}></Route>
            <Route path="/JPHeckPages/WorkExperience" element={<WorkExperience />}></Route>
            <Route path="/JPHeckPages/HonorsAndAwards" element={<HonorsAndAwards />}></Route>
        </Routes>
    );
}

export default Main;
