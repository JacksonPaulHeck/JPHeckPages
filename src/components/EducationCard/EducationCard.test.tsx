import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import EducationCard from './EducationCard';
import { EducationItem } from '../../pages/EducationAndCredentials/EducationAndCredentials';

describe('<EducationCard />', () => {
  test('it should mount', () => {
    let item: EducationItem = {
      id: 0,
      name: "",
      degree: [],
      location: "",
      time: "",
    };
    render(<EducationCard key={item.id} item={item} />);

    const educationCard = screen.getByTestId('EducationCard');

    expect(educationCard).toBeInTheDocument();
  });
});