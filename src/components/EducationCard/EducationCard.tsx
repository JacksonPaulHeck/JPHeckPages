import { Accordion, AccordionDetails, AccordionSummary, Card, CardContent, Divider, Typography } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { FC } from 'react';
import './EducationCard.scss';
import { EducationItem } from '../../pages/EducationAndCredentials/EducationAndCredentials';
import { useTranslation } from 'react-i18next';

interface EducationCardProps {
  key: number
  item: EducationItem
}

const EducationCard: FC<EducationCardProps> = (props) => {
  const { t } = useTranslation();

  return (
    <Card>
      <CardContent>
        <Typography gutterBottom>
          {props.item.name}
        </Typography>
        <Typography>
          {props.item.location}
        </Typography>
        <Typography>
          {props.item.time}
        </Typography>
        <Divider />
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1-content"
            id="panel1-header"
          >
            {t('General.MoreDetails')}
          </AccordionSummary>
          <AccordionDetails>
            {props.item?.degree?.map(content => (<Typography key={content.id}>{content.text}</Typography>))}
          </AccordionDetails>
        </Accordion>
      </CardContent>
    </Card>
  );
};

export default EducationCard;
