import { Paper, Typography } from '@mui/material';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';

interface WorkInProgressProps { }

interface content {
  id: number,
  text: string
}

const WorkInProgress: FC<WorkInProgressProps> = () => {
  const { t } = useTranslation();

  const pageContent: content[] = t('WorkInProgress.Content', { returnObjects: true });

  return (
    <Paper>
      {pageContent.map(item => (<Typography key={item.id}>{item.text}</Typography>))}
    </Paper >
  );
}

export default WorkInProgress;
