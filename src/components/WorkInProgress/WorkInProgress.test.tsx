import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import WorkInProgress from './WorkInProgress';

describe('<WorkInProgress />', () => {
  test('it should mount', () => {
    render(<WorkInProgress />);

    const home = screen.getByTestId('WorkInProgress');

    expect(home).toBeInTheDocument();
  });
});