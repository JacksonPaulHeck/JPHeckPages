import { Card, CardContent, Divider, Typography } from '@mui/material';
import { FC } from 'react';
import './CredentialsCard.scss';
import { CredentialsItem } from '../../pages/EducationAndCredentials/EducationAndCredentials';

interface CredentialsCardProps {
  key: number
  item: CredentialsItem
}

const CredentialsCard: FC<CredentialsCardProps> = (props) => {
  return (
    <Card>
      <CardContent>
        <Typography gutterBottom>
          {props.item.name}
        </Typography>
        <Typography>
          {props.item.location}
        </Typography>
        <Typography>
          {props.item.time}
        </Typography>
        <Divider />
      </CardContent>
    </Card>
  );
};

export default CredentialsCard;
