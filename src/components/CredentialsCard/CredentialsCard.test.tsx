import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import CredentialsCard from './CredentialsCard';
import { CredentialsItem } from '../../pages/EducationAndCredentials/EducationAndCredentials';

describe('<CredentialsCard />', () => {
  test('it should mount', () => {
    let item: CredentialsItem = {
      id: 0,
      name: "",
      location: "",
      time: ""
    };
    render(<CredentialsCard key={item.id} item={item} />);

    const credentialsCard = screen.getByTestId('CredentialsCard');

    expect(credentialsCard).toBeInTheDocument();
  });
});