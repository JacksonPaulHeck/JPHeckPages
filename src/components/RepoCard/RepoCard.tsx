import { Button, Card, CardActions, CardContent, Typography } from '@mui/material';
import { FC } from 'react';
import { Repository } from '../../pages/Projects/Projects';
import './RepoCard.scss';
import { useTranslation } from 'react-i18next';

interface RepoCardProps {
  key: number
  item: Repository
}

const RepoCard: FC<RepoCardProps> = (props) => {
  const { t } = useTranslation();

  return (
    <Card sx={{ width: 275, flex: "0 1 calc(25% - 1em)", boxSizing: "border-box", margin: "1rem .25em" }}>
      <CardContent>
        <Typography gutterBottom>
          {props.item.name}
        </Typography>
        <Typography>
          {props.item.description}
        </Typography>
      </CardContent>
      <CardActions>
        <Button href={props.item.web_url}>{t('General.LearnMore')}</Button>
      </CardActions>
    </Card>
  );
};

export default RepoCard;
