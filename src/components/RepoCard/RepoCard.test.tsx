import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import { Repository } from '../../pages/Projects/Projects';
import RepoCard from './RepoCard';

describe('<RepoCard />', () => {
  test('it should mount', () => {
    let repo: Repository = { id: 0, name: "", web_url: "", description: "" };

    render(<RepoCard key={repo.id} item={repo} />);

    const repoCard = screen.getByTestId('RepoCard');

    expect(repoCard).toBeInTheDocument();
  });
});