import { Accordion, AccordionDetails, AccordionSummary, Card, CardContent, Divider, Typography } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { FC } from 'react';
import './WorkCard.scss';
import { WorkItem } from '../../pages/WorkExperience/WorkExperience';
import { useTranslation } from 'react-i18next';

interface WorkCardProps {
  key: number
  item: WorkItem
}

const WorkCard: FC<WorkCardProps> = (props) => {
  const { t } = useTranslation();

  return (
    <Card>
      <CardContent>
        <Typography gutterBottom>
          {props.item.company}
        </Typography>
        <Typography>
          {props.item.title}
        </Typography>
        <Typography>
          {props.item.location}
        </Typography>
        <Typography>
          {props.item.time}
        </Typography>
        <Divider />
        <Accordion>
          <AccordionSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1-content"
            id="panel1-header"
          >
            {t('General.MoreDetails')}
          </AccordionSummary>
          <AccordionDetails>
            {props.item?.notes?.map(content => (<Typography key={content.id}>{content.text}</Typography>))}
          </AccordionDetails>
        </Accordion>
      </CardContent>
    </Card>
  );
};

export default WorkCard;
