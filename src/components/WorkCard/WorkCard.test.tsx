import '@testing-library/jest-dom/extend-expect';
import { render, screen } from '@testing-library/react';
import WorkCard from './WorkCard';
import { WorkItem } from '../../pages/WorkExperience/WorkExperience';

describe('<WorkCard />', () => {
  test('it should mount', () => {
    let item: WorkItem = {
      id: 0,
      company: "",
      title: "",
      location: "",
      time: "",
      notes: []
    };
    render(<WorkCard key={item.id} item={item} />);

    const workCard = screen.getByTestId('WorkCard');

    expect(workCard).toBeInTheDocument();
  });
});