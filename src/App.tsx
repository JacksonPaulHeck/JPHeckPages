import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider, createTheme } from '@mui/material/styles';
import './App.scss';
import Footer from './components/Footer/Footer';
import Main from './components/Main/Main';
import Navbar from './components/Navbar/Navbar';
import { Breadcrumbs, Link, Typography } from '@mui/material';
import { LinkProps } from '@mui/material/Link';
import {
  Link as RouterLink,
  useLocation,
} from 'react-router-dom';
import { useTranslation } from 'react-i18next';

const darkTheme = createTheme({
  palette: {
    mode: 'dark',
  },
});



interface LinkRouterProps extends LinkProps {
  to: string;
  replace?: boolean;
}

function LinkRouter(props: LinkRouterProps) {
  return <Link {...props} component={RouterLink as any} />;
}

function BasicBreadcrumbs() {
  const location = useLocation();
  const pathnames = location.pathname.split('/').filter((x) => x);
  const { t } = useTranslation();

  const breadcrumbNameMap: { [key: string]: string } = {
    '/JPHeckPages': 'JPHeckPages',
    '/JPHeckPages/Projects': t('Navbar.Projects'),
    '/JPHeckPages/Activities': t('Navbar.Activities'),
    '/JPHeckPages/CommunityAndLeadership': t('Navbar.Community'),
    '/JPHeckPages/EducationAndCredentials': t('Navbar.Education'),
    '/JPHeckPages/HonorsAndAwards': t('Navbar.Honors'),
    '/JPHeckPages/WorkExperience': t('Navbar.Work')
  };
  return (
    <Breadcrumbs aria-label="breadcrumb">
      <LinkRouter underline="hover" color="inherit" to="/JPHeckPages">
        Home
      </LinkRouter>
      {pathnames.map((value, index) => {
        const last = index === pathnames.length - 1;
        const to = `/${pathnames.slice(0, index + 1).join('/')}`;
        return last ? (
          <Typography color="text.primary" key={to}>
            {breadcrumbNameMap[to]}
          </Typography>
        ) : (
          <LinkRouter underline="hover" color="inherit" to={to} key={to}>
            {breadcrumbNameMap[to]}
          </LinkRouter>
        );
      })}
    </Breadcrumbs>
  );
}

function App() {

  return (
    <ThemeProvider theme={darkTheme}>
      <CssBaseline />
      <div className="App">
        <Navbar />
        <BasicBreadcrumbs />
        <Main />
        <Footer />
      </div>
    </ThemeProvider>

  );
}

export default App;


