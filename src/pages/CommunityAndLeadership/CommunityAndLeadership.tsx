import { FC } from 'react';
import WorkInProgress from '../../components/WorkInProgress/WorkInProgress';
interface CommunityAndLeadershipProps { }

const CommunityAndLeadership: FC<CommunityAndLeadershipProps> = () => (
    <WorkInProgress />
);

export default CommunityAndLeadership;
