import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import CommunityAndLeadership from './CommunityAndLeadership';

describe('<CommunityAndLeadership />', () => {
  test('it should mount', () => {
    render(<CommunityAndLeadership />);
    
    const communityAndLeadership = screen.getByTestId('CommunityAndLeadership');

    expect(communityAndLeadership).toBeInTheDocument();
  });
});