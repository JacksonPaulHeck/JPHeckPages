import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import EducationCard from '../../components/EducationCard/EducationCard';
import CredentialsCard from '../../components/CredentialsCard/CredentialsCard';

export interface DegreeItem {
  id: number
  text: string
}

export interface CredentialsItem {
  id: number
  name: string
  time: string
  location: string
}

export interface EducationItem {
  id: number
  name: string
  degree: DegreeItem[]
  time: string
  location: string
}

interface EducationAndCredentialsProps { }

const EducationAndCredentials: FC<EducationAndCredentialsProps> = () => {
  const { t } = useTranslation();

  const educationItems: EducationItem[] = t('Education.Schools', { returnObjects: true })
  const credentialsItems: CredentialsItem[] = t('Credentials', { returnObjects: true })

  return (
    <div className='education-and-credentials'>
      {educationItems?.map((item) => (<EducationCard key={item.id} item={item} />))}
      {credentialsItems?.map((item) => (<CredentialsCard key={item.id} item={item} />))}
    </div>
  );
};

export default EducationAndCredentials;
