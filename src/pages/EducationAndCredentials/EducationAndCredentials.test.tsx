import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import EducationAndCredentials from './EducationAndCredentials';

describe('<EducationAndCredentials />', () => {
  test('it should mount', () => {
    render(<EducationAndCredentials />);
    
    const educationAndCredentials = screen.getByTestId('EducationAndCredentials');

    expect(educationAndCredentials).toBeInTheDocument();
  });
});