import axios, { AxiosResponse } from 'axios';
import { useEffect, useState } from 'react';
import RepoCard from '../../components/RepoCard/RepoCard';
import { FC } from 'react';

export interface Repository {
    id: number,
    name: string,
    description: string,
    web_url: string,
}

interface ProjectsProps { }

const Projects: FC<ProjectsProps> = () => {
    const repoUserName = "13551849";
    const [arrayItems, setArrayItems] = useState<Repository[]>([]);

    useEffect(() => {
        axios.get(`https://gitlab.com/api/v4/users/${repoUserName}/projects`)
            .then((res: AxiosResponse<any, any>) => {
                setArrayItems(res.data);
            }).catch((err) => {
                setArrayItems([]);
            }).finally(() => { });
    }, []);


    return (
        <div className='projects'>
            {arrayItems?.map((item) => (
                <RepoCard key={item.id} item={item} />
            ))}
        </div>
    );
}

export default Projects;
