import { FC } from 'react';
import { useTranslation } from 'react-i18next';
import WorkCard from '../../components/WorkCard/WorkCard';

interface WorkExperienceProps { }

export interface NotesItem {
  id: number
  text: string
}

export interface WorkItem {
  id: number
  company: string
  title: string
  location: string
  time: string
  notes: NotesItem[]
}

const WorkExperience: FC<WorkExperienceProps> = () => {

  const { t } = useTranslation();

  const workItems: WorkItem[] = t('Work.Items', { returnObjects: true });

  return (
    <div className='work-experience'>
      {workItems.map((item) => (<WorkCard key={item.id} item={item} />))}
    </div>
  );
};

export default WorkExperience;
