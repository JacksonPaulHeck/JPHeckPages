import { FC } from 'react';
import WorkInProgress from '../../components/WorkInProgress/WorkInProgress';

interface HonorsAndAwardsProps { }

const HonorsAndAwards: FC<HonorsAndAwardsProps> = () => (
  <WorkInProgress />
);

export default HonorsAndAwards;
