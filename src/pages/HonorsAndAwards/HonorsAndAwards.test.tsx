import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import HonorsAndAwards from './HonorsAndAwards';

describe('<HonorsAndAwards />', () => {
  test('it should mount', () => {
    render(<HonorsAndAwards />);
    
    const honorsAndAwards = screen.getByTestId('HonorsAndAwards');

    expect(honorsAndAwards).toBeInTheDocument();
  });
});