import { Box, Paper, Typography } from '@mui/material';
import { FC } from 'react';
import { useTranslation } from 'react-i18next';

interface HomeProps { }

interface content {
  id: number,
  text: string
}

const Home: FC<HomeProps> = () => {
  const { t } = useTranslation();

  const pageContent: content[] = t('Home.Content', { returnObjects: true });

  return (
    <Paper>
      <Typography variant="h1">{t('Home.Name')}</Typography>
      <Typography variant="h4">{t('Home.Phone')} • {t('Home.Email')}</Typography>
      <Paper sx={{
        display: 'flex',
        justifyContent: 'space-around'
      }}>
        <Box sx={{
          padding: "1em",
        }}>
          {pageContent.map((item) => (
            <Typography key={item.id} variant="h6" sx={{}} align='left'>{item.text}</Typography>
          ))}
        </Box>
      </Paper>
    </Paper>
  );
}

export default Home;
