import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect';
import Activities from './Activities';

describe('<Activities />', () => {
  test('it should mount', () => {
    render(<Activities />);
    
    const activities = screen.getByTestId('Activities');

    expect(activities).toBeInTheDocument();
  });
});