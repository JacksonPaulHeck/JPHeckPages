import { FC } from 'react';
import WorkInProgress from '../../components/WorkInProgress/WorkInProgress';

interface ActivitiesProps { }

const Activities: FC<ActivitiesProps> = () => (
    <WorkInProgress />
);

export default Activities;
