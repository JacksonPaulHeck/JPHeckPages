FROM node:latest as build

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY . .
RUN npm install
RUN npm run build

FROM nginx:latest
EXPOSE 80
COPY --from=build /app/build /usr/share/nginx/html

